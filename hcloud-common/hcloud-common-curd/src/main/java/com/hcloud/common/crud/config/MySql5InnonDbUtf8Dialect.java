package com.hcloud.common.crud.config;

import org.hibernate.dialect.MySQL5InnoDBDialect;

/**
 * 解决jpa自动建表时数据库引擎有问题以及字符集出问题
 * @Auther hepangui
 * @Date 2018/11/8
 */
public class MySql5InnonDbUtf8Dialect extends MySQL5InnoDBDialect {
    @Override
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
    }
}
