package com.tx.txinitiator.service.impl;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.codingapi.txlcn.tc.annotation.TxcTransaction;
import com.tx.txinitiator.entity.TxInitiatorEntity;
import com.tx.txinitiator.feign.RemoteResponder;
import com.tx.txinitiator.service.TxInitiatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;


@Service
public class InitiatorService {

  @Autowired
  RemoteResponder remoteResponder;


  @Autowired
  TxInitiatorService txInitiatorService;


    @LcnTransaction
//    @TxcTransaction//分布式事务注解
    @Transactional
    public  String   execute(){
      String test = remoteResponder.rpc("test");
      txInitiatorService.insert(TxInitiatorEntity.builder().id(UUID.randomUUID().toString()).name("test").build());
      return   test+ "return";
    }
}
