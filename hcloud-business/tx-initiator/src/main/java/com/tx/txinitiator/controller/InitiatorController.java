package com.tx.txinitiator.controller;

import com.tx.txinitiator.service.impl.InitiatorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class InitiatorController {

   @Resource
    InitiatorService initiatorService;


    @GetMapping("/execute")
    public String execute(){
        String execute = initiatorService.execute();
        return  "ok"+execute;
    }



}
