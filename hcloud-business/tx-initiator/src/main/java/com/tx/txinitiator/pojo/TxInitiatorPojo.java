package com.tx.txinitiator.pojo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.LocalTime;


import com.common.bean.Entity;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Builder;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:55
 */
@Table(name = "tx_initiator")
@Builder
@Getter
@Setter
public class TxInitiatorPojo implements Entity {
    private static final long serialVersionUID = 1L;
    @Id
          /**
      *
       */
              private String id;
          /**
      *
       */
              private String name;

    public TxInitiatorPojo() {
    }

    ;

    public TxInitiatorPojo(   String id ,   String name  ) {
                    this.id= id;
                    this.name= name;
            }

    ;

  @Override
  @SneakyThrows
  public TxInitiatorPojo clone() {
    return (TxInitiatorPojo) super.clone();
  }

}
