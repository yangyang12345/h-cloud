package com.tx.txinitiator.mapper;

import com.tx.txinitiator.entity.TxInitiatorEntity;
import com.tx.txinitiator.pojo.TxInitiatorPojo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Map;
import java.util.List;
/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:55
 */
@Mapper
public interface TxInitiatorMapper   extends tk.mybatis.mapper.common.Mapper<TxInitiatorEntity> {

  public List<TxInitiatorPojo> queryList(Map<String, Object> map);
  public List<TxInitiatorPojo> queryObject(String id);
  public List<TxInitiatorEntity> selectBySchoolId(String schools);
  public void deleteBatch(@Param("array") List<String> array);
}
