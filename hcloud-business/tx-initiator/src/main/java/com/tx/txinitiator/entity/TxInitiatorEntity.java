package com.tx.txinitiator.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.LocalTime;


import com.common.bean.Entity;
import lombok.Builder;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Min;
import com.alibaba.fastjson.annotation.JSONField;

/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:55
 */
@Table(name = "tx_initiator")
@Builder
@Getter
@Setter
public class TxInitiatorEntity implements Entity {
    private static final long serialVersionUID = 1L;
    @Id
          /**
      *
       */
        @NotNull
                @NotBlank
        @Length(min=0, max=100)
                private String id;
          /**
      *
       */
        @NotNull
                @NotBlank
        @Length(min=0, max=100)
                private String name;

    public TxInitiatorEntity() {
    }

    ;

    public TxInitiatorEntity(   String id ,   String name  ) {
                    this.id= id;
                    this.name= name;
            }

    ;

  @Override
  @SneakyThrows
  public TxInitiatorEntity clone() {
    return (TxInitiatorEntity) super.clone();
  }

}
