package com.tx.txinitiator.service.impl;

import com.common.service.impl.BaseServiceImpl;
import com.common.util.Pagination;
import com.github.pagehelper.PageHelper;
import com.tx.txinitiator.entity.TxInitiatorEntity;
import com.tx.txinitiator.mapper.TxInitiatorMapper;
import com.tx.txinitiator.pojo.TxInitiatorPojo;
import com.tx.txinitiator.service.TxInitiatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;




@Service("txInitiatorService")
@Transactional(readOnly = false, rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
public class TxInitiatorServiceImpl extends BaseServiceImpl<TxInitiatorMapper, TxInitiatorEntity> implements TxInitiatorService {

  @Override
  @Transactional(propagation=Propagation.NOT_SUPPORTED)
  public List<TxInitiatorPojo> queryList(Map<String, Object> map, Pagination pagination){
    PageHelper.startPage(pagination.getPageNum(), pagination.getPageSize());
    return mapper.queryList(map);
  }

  @Override
  public  List<TxInitiatorEntity> selectBySchoolId(String schools) {
    return mapper.selectBySchoolId(schools);
  }

  @Override
  public void deleteBatch(List<String> array) {
    mapper.deleteBatch( array );
  }
}
