package com.tx.txinitiator.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "responder")
public interface RemoteResponder {

    @GetMapping("/execute/{value}")
    public String rpc(@PathVariable("value") String value );

}
