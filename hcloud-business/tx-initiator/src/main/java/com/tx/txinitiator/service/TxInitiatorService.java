package com.tx.txinitiator.service;


import com.common.service.BaseService;
import com.common.util.Pagination;
import com.tx.txinitiator.entity.TxInitiatorEntity;
import com.tx.txinitiator.pojo.TxInitiatorPojo;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:55
 */
public interface TxInitiatorService  extends BaseService<TxInitiatorEntity> {

  public List<TxInitiatorPojo> queryList(Map<String, Object> map, Pagination pagination);

  List< TxInitiatorEntity> selectBySchoolId(String schools);

  public void deleteBatch(List<String> array);
}
