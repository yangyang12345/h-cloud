package com.common.util;

import com.common.bean.Entity;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/3/22 15:13 2019 03
 */
@Configuration
@Component
public class BaseEntitySetter {

  @Autowired
  CurrentUserInfo currentUserInfo;


  public  void insert(@NonNull Entity baseEntity){
    baseEntity.setSchoolId(currentUserInfo.getCurrentSchoolId());
    BaseEntityUtils.insert(baseEntity);
  }


  public  void update(@NonNull Entity baseEntity){
    baseEntity.setSchoolId(currentUserInfo.getCurrentSchoolId());
    BaseEntityUtils.insert(baseEntity);
  }
}
