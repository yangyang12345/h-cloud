package com.common.util.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2018/11/15 9:12 2018 11JsonSerialize
 * 将接收的前端字符串类型转换成LocalDateTime类型
 */
@Slf4j
public class ToLocalTimeDeserializer extends JsonDeserializer<LocalTime> {

  @Override
  public LocalTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    String value = jsonParser.getText();
    try {
      boolean blank = StringUtils.isBlank(value);
      if (blank) {
        return null;
      }
      DateTimeFormatter format =  DateTimeFormatter.ofPattern("HH:mm:ss");
      return LocalTime.parse(value,format);

    } catch (NumberFormatException e) {
      log.error("数据转换异常", e);
      return null;
    }
  }


}
