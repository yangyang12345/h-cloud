package com.common.util.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2018/9/25 9:07 2018 09
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Pagination {

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate gmtEndDate;
  private LocalDateTime gmtEndDateTime;
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate gmtStartDate;
  private LocalDateTime gmtStartDateTime;
  private Integer limit;
  @NotNull(message = "每页多少条数不能为null")
  private Integer pageNum;
  private Integer offset;
  @NotNull(message = "页数不能为null")
  private Integer pageSize;
  /**
   * 根据啥排序
   */
  private String sidx;

  public void localDateToLocaDateTime() {
    if (gmtEndDate != null) {

      this.gmtEndDateTime = LocalDateTime.of(gmtEndDate, LocalTime.MAX);
    }
    if (gmtStartDate != null) {

      this.gmtStartDateTime = LocalDateTime.of(gmtStartDate, LocalTime.MIN);
    }
  }

  public void paginationPlus() {
    if (this.pageSize == null || this.pageNum == null) {
      this.pageNum=1;
      this.pageSize=10;
    }
  }


  public void pagination() {
    if (this.pageSize != null && this.pageNum != null) {
      Integer page = this.getPageSize();
      Integer number = this.getPageNum();
      int value = (page - 1) * number;
      this.offset = value < 0 ? 0 : value;
      this.limit = number;
    }else{
      this.pageNum=1;
      this.pageSize=10;
    }
  }


}
