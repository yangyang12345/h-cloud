package com.common.exception;


/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/3/22 11:05 2019 03
 */
public class ParameterException extends RuntimeException {
  public ParameterException(String message) {
    super(message);
  }
}
