package com.common.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/4/1 11:57 2019 04
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=CertificateValidator.class)
public @interface Certificate {

  String message() default"不是证书形式";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
