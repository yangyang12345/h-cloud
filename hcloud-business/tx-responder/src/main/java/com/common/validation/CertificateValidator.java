package com.common.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/4/1 11:59 2019 04
 */
public class CertificateValidator implements ConstraintValidator<Certificate, String> {

   private String moneyReg = "^\\d+(\\.\\d{1,2})?$";//表示金额的正则表达式
  private Pattern moneyPattern = Pattern.compile(moneyReg);
  /**
   * Implements the validation logic.
   * The state of {@code value} must not be altered.
   * <p>
   * This method can be accessed concurrently, thread-safety must be ensured
   * by the implementation.
   *
   * @param value   object to validate
   * @param context context in which the constraint is evaluated
   * @return {@code false} if {@code value} does not pass the constraint
   */
  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    // TODO Auto-generated method stub
    if (value == null)
      return true;
    return moneyPattern.matcher(value.toString()).matches();
  }
}
