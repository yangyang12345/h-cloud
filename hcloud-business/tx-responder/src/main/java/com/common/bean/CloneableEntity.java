package com.common.bean;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/3/22 15:16 2019 03
 */
public interface CloneableEntity extends Cloneable {
       CloneableEntity clone();
}
