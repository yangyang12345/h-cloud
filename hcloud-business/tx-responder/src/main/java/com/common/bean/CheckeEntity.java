package com.common.bean;


import com.common.util.util.BaseEntityUtils;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/3/22 15:17 2019 03
 */
public interface CheckeEntity extends BaseEntity{


  default void check(){
   if(this.getStatus()==null){
     BaseEntityUtils.query(this);
   }
  }
  default void defaultCheck(Byte status){
   status = BaseEntityUtils.check(status);
  }

}
