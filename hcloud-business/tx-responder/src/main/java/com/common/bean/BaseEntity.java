package com.common.bean;

import java.time.LocalDateTime;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/3/22 10:35 2019 03
 */
public interface BaseEntity  extends CloneableEntity {



  /**
   * 设置：gmt_create
   */
  default void setGmtCreate(LocalDateTime gmtCreate) {
  }


  /**
   * 获取：gmt_create
   */
  default LocalDateTime getGmtCreate() {
    return LocalDateTime.now();
  }


  /**
   * 设置：gmt_modified
   */
  default void setGmtModified(LocalDateTime gmtModified) {
  }


  /**
   * 获取：gmt_modified
   */
  default LocalDateTime getGmtModified() {
    return LocalDateTime.now();
  }


  /**
   * 设置：主键
   */
  default void setId(String id) {
  }


  /**
   * 获取：主键
   */
  default String getId() {
    return null;
  }



  /**
   * 设置：数据状态  -1已删除 0草稿 1审核中  2正常 3 审核未通过
   */
  default void setStatus(Byte status) {
  }


  /**
   * 获取：数据状态  -1已删除 0草稿 1审核中  2正常 3 审核未通过
   */
  default Byte getStatus() {
    return null;
  }


  /**
   * 设置：备注
   */
  default void setRemarks(String remarks) {
  }


  /**
   * 获取：备注
   */
  default String getRemarks() {
    return null;
  }


  /**
   * 设置：学校id
   */
  default void setSchoolId(String schoolId) {
  }

  /**
   * 获取：学校id
   */
  default String getSchoolId() {
    return null;
  }



  default Byte getExpire() {
    return null;
  }

  default void setExpire(Byte expire) {
  }

  default Long getCreateId() {
   return  null;
  }

  default void setCreateId(Long createId) {

  }

  default Long getModifiedId() {
  return null;
  }

  default void setModifiedId(Long modifiedId) {

  }
}
