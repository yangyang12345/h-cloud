package com.common.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2018/11/15 9:12 2018 11JsonSerialize
 * 将接收的前端字符串类型转换成LocalDateTime类型
 */
@Slf4j
public class ToLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

  @Override
  public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    String value = jsonParser.getText();
    try {
      boolean blank = StringUtils.isBlank(value);
      if (blank) {
        return null;
      }
      DateTimeFormatter format =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
      return LocalDateTime.parse("2017-09-28 17:07:05",format);

    } catch (NumberFormatException e) {
      log.error("数据转换异常", e);
      return null;
    }
  }


}
