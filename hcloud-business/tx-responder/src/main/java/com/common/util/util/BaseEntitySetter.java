package com.common.util.util;

import com.common.bean.Entity;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/3/22 15:13 2019 03
 */
@Configuration
@Component
public class BaseEntitySetter {

  @Autowired
  CurrentUserInfo currentUserInfo;
//  @Autowired
//  GenerateIdFctory generateIdFctory;

  static {

  }

  /**
   *    设置  order_no  以及是否过期
   * @param baseEntity
   */
  public void setParam(@NonNull Entity baseEntity, String mark){
    // @TODO   数据库  获取  order_no  获取  有并发问题
    baseEntity.setExpire(BaseEntityUtils.ExpireEnum.NORMAL.getStatus());
  }


  public  void insert(@NonNull Entity baseEntity){
    createInfo(baseEntity);
    updateInfo(baseEntity);
    BaseEntityUtils.insert(baseEntity);
    baseEntity.setId(this.getGenerateId());
  }


  public String getGenerateId() {
//    return generateIdFctory.getGenerateId();
    return UUID.fromString("asda").toString();
  }


  public  void update(@NonNull Entity baseEntity){
    updateInfo(baseEntity);
    baseEntity.setCreateId(null);
    BaseEntityUtils.update(baseEntity);
  }


  private void createInfo(@NonNull Entity baseEntity) {
//    PlatformUser currentUserInfo = (PlatformUser) this.currentUserInfo.getCurrentUserInfo();
//    baseEntity.setSchoolId(currentUserInfo.getSchoolId());
//    baseEntity.setCreateId(currentUserInfo.getId());
  }


  private void updateInfo(@NonNull Entity baseEntity) {
//    PlatformUser currentUserInfo = (PlatformUser) this.currentUserInfo.getCurrentUserInfo();
//    baseEntity.setSchoolId(currentUserInfo.getSchoolId());
//    baseEntity.setModifiedId(currentUserInfo.getId());
  }


  public void  query(@NonNull Entity baseEntity){
    BaseEntityUtils.query(baseEntity);
  }


//  public PlatformUserInf currentUserInfo(){
//   return (PlatformUserInf)currentUserInfo.getCurrentUserInfo();
//  }


//  public PlatformUserInf currentUserInfo(Long id){
//   return (PlatformUserInf)currentUserInfo.getCurrentUserInfo(id);
//  }


//  public List<PlatformUserInf> queryPlatformUsers(String departmentId, String schoolid){
//    List<PlatformUserInf> platformUserInfs =new ArrayList<>(1);
//    PlatformUserInf platformUserInf = currentUserInfo(1L);
//    platformUserInfs.add(platformUserInf);
//    return  platformUserInfs;
//  }


}
