package com.common.util;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Result implements Serializable, Cloneable {

  private static final String DEFAULT_FAIL = "fail";
  private static final int DEFAULT_FAIL_STATUS = 500;
  private static final String DEFAULT_OK = "success";
  private static final int DEFAULT_OK_STATUS = 200;

  //结果对象
  private Object data;

  @JsonIgnore()
  @JSONField(serialize = false)
  private int isHash = 0;
  //失败消息
  private String errorMsg;
  //状态码
  private Integer error;

  public Result() {
  }


  public static Result error(String message, Object result) {
    Result r = new Result();
    r.setError(DEFAULT_FAIL_STATUS);
    r.setErrorMsg(message);
    r.setData(result);
    return r;
  }


  public static Result error() {
    return Result.error(DEFAULT_FAIL);
  }


  /**
   * @param message
   * @desc: 未知异常 状态码固定为500， 返回异常消息
   * @return:
   */
  public static Result error(String message) {
    return Result.error(DEFAULT_FAIL_STATUS, message);
  }

  /**
   *   只返回状态码
   * @param status
   * @return
   */
  public static Result error(int status) {
    return Result.error(status, DEFAULT_FAIL);
  }


  /**
   * @param
   * @desc: 已知异常, 返回该异常状态码及异常消息
   * @return:
   */
  public static Result error(Integer statusCode, String message) {
    Result r = new Result();
    r.setError(statusCode);
    r.setErrorMsg(message);
    return r;
  }


  public static Result ok(Integer statusCode, Object result) {
    return Result.ok(statusCode, DEFAULT_OK, result);
  }


  public static Result ok(Integer statusCode, String message, Object result) {
    Result r = new Result();
    r.setError(statusCode);
    r.setErrorMsg(message);
    r.setData(result);
    return r;
  }


  public static Result ok(String msg, Object result) {
    return Result.ok(DEFAULT_OK_STATUS, msg, result);
  }


  /**
   * @param
   * @desc: 成功返回
   * @return: Result
   */
  public static Result ok() {
    return Result.ok(DEFAULT_OK_STATUS, "success", null);
  }


  public static Result ok(Object result) {
    return Result.ok(DEFAULT_OK_STATUS, "success", result);
  }


  public String getErrorMsg() {
    return errorMsg;
  }

  public void setErrorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
  }

  public Integer getError() {
    return error;
  }

  public void setError(Integer error) {
    this.error = error;
  }

  public Result put(String key, Object value) {
    if (1 == isHash) {
      ((HashMap) data).put(key, value);
      return this;
    }
    if (Objects.isNull(this.data)) {
      castHashMap(key, value);
    } else if (!(data instanceof HashMap)) {
      Result result = this.clone();
      castHashMap(result, key, value);
    }
    return this;
  }


  private void castHashMap(String key, Object value) {
    final Map map = getMap();
    if (Objects.nonNull(key)) {
      map.put(key, value);
    }
    data = map;
    isHash = 1;
  }


  //使用clone()方法实现浅克隆
  @Override
  public Result clone() {
    Object obj = null;
    try {
      obj = super.clone();
      return (Result) obj;
    } catch (CloneNotSupportedException e) {
      System.out.println("不支持复制！");
      return null;
    }
  }


  private void castHashMap(Result result, String key, Object value) {
    final Map map = getMap();
    final Object datac = result.getData();
    if (Objects.nonNull(datac)) {
      map.put(key, value);
      map.put("object", datac);
    }
    data = map;
    isHash = 1;
  }


  private Map getMap() {
    try {
      return HashMap.class.newInstance();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
    return null;
  }


  public Object getData() {
    return data;
  }


  public void setData(Object data) {
    this.data = data;
  }


}
