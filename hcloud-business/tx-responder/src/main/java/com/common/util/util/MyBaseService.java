package com.common.util.util;

import com.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * @author pengzhen
 * @email pengzhen
 * @date 2019/3/25 17:52 2019 03
 */
public interface MyBaseService<T,X> extends BaseService<T> {

  public List<X> queryList(Map<String, Object> map, Pagination pagination);


  List<T> selectBySchoolId(String schools);

  public void deleteBatch(List<Long> array);
}
