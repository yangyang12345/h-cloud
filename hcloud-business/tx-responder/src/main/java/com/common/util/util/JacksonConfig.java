package com.common.util.util;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: leichengzhi   2019-09-28 20:08:55
 * @email: leichengzhi@bstyjy.cn
 * @Date: 2018/12/29 15:43
 * @Version 1.0
 */
@Slf4j
@Configuration
public class JacksonConfig {

//  @Bean
//  @Primary
//  @ConditionalOnMissingBean(ObjectMapper.class)
//  public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder) {
//    ObjectMapper objectMapper = builder.createXmlMapper(false).build();
//    JavaTimeModule javaTimeModule = new JavaTimeModule();
//    javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//    javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
//    javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ofPattern("HH:mm:ss")));
//    objectMapper.registerModule(javaTimeModule);
//
//    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//    return objectMapper;
//  }

  /**
   * 覆盖方法configureMessageConverters，使用fastJson
   * @return
   */
  @Bean
  @Primary
  public HttpMessageConverters fastJsonHttpMessageConverters() {
    //1、定义一个convert转换消息的对象
    FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();

    log.info("converters:" + fastConverter.toString());
    List<MediaType> supportMediaTypeList = new ArrayList<>();
//        supportMediaTypeList.add(MediaType.TEXT_HTML);
    supportMediaTypeList.add(MediaType.APPLICATION_JSON_UTF8);
//        supportMediaTypeList.add(MediaType.IMAGE_GIF);
//        supportMediaTypeList.add(MediaType.IMAGE_JPEG);
//        supportMediaTypeList.add(MediaType.IMAGE_PNG);

    //2、添加fastjson的配置信息
    FastJsonConfig config = new FastJsonConfig();
    config.setSerializerFeatures(
        SerializerFeature.WriteMapNullValue,//保留空的字段
        SerializerFeature.WriteNullStringAsEmpty,//String null -> ""
        SerializerFeature.WriteNullNumberAsZero,//Number null -> 0
        SerializerFeature.WriteNullListAsEmpty,//List null-> []
        SerializerFeature.WriteNullBooleanAsFalse);//Boolean null -> false
    //3、在convert中添加配置信息
    fastConverter.setFastJsonConfig(config);
    //4、将convert添加到converters中
    HttpMessageConverter<?> converter = fastConverter;
    return new HttpMessageConverters(converter);
  }

}
