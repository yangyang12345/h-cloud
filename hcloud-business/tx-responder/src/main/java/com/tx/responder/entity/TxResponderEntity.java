package com.tx.responder.entity;

import com.common.bean.Entity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:54
 */
@Table(name = "tx_responder")
@Builder
@Getter
@Setter
public class TxResponderEntity implements Entity {
    private static final long serialVersionUID = 1L;
    @Id
          /**
      *
       */
        @NotNull
                @NotBlank
        @Length(min=0, max=100)
                private String id;
          /**
      *
       */
        @NotNull
                @NotBlank
        @Length(min=0, max=100)
                private String testData;

    public TxResponderEntity() {
    }

    ;

    public TxResponderEntity(   String id ,   String testData  ) {
                    this.id= id;
                    this.testData= testData;
            }

    ;

  @Override
  @SneakyThrows
  public TxResponderEntity clone() {
    return (TxResponderEntity) super.clone();
  }

}
