package com.tx.responder.service.impl;

import com.common.service.impl.BaseServiceImpl;
import com.common.util.Pagination;
import com.github.pagehelper.PageHelper;
import com.tx.responder.entity.TxResponderEntity;
import com.tx.responder.mapper.TxResponderMapper;
import com.tx.responder.pojo.TxResponderPojo;
import com.tx.responder.service.TxResponderService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("txResponderService")
@Transactional(readOnly = false, rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
public class TxResponderServiceImpl extends BaseServiceImpl<TxResponderMapper, TxResponderEntity> implements TxResponderService {

  @Override
  @Transactional(propagation=Propagation.NOT_SUPPORTED)
  public List<TxResponderPojo> queryList(Map<String, Object> map, Pagination pagination){
    PageHelper.startPage(pagination.getPageNum(), pagination.getPageSize());
    return mapper.queryList(map);
  }

  @Override
  public  List<TxResponderEntity> selectBySchoolId(String schools) {
    return mapper.selectBySchoolId(schools);
  }

  @Override
  public void deleteBatch(List<String> array) {
    mapper.deleteBatch( array );
  }
}
