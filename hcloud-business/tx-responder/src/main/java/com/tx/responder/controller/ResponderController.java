package com.tx.responder.controller;

import com.tx.responder.service.ResponderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponderController {

    @Autowired
    ResponderService responderService;

    @GetMapping("/execute/{value}")
    public String execute(@PathVariable("value") String value ){
        String execute = null;
        try {
            execute = responderService.rpc(value);
        } catch (Exception e) {
//            e.printStackTrace();
            System.out.println("asadsad====================================");
            return  "error";
        }
        return  "ok"+execute;
    }


}
