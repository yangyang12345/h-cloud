package com.tx.responder;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@SpringBootApplication
@EnableDiscoveryClient
@Configuration
@EnableDistributedTransaction
public class TxResponderApplication {

    public static void main(String[] args) {
        SpringApplication.run(TxResponderApplication.class, args);
    }

//    @ConfigurationProperties(prefix = "spring.datasource")
//    @Bean
//    public DataSource dataSource(){
//    return     new HikariDataSource();
//    }

}
