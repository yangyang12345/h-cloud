package com.tx.responder.service;


import com.common.service.BaseService;
import com.common.util.Pagination;
import com.tx.responder.entity.TxResponderEntity;
import com.tx.responder.pojo.TxResponderPojo;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:54
 */
public interface TxResponderService  extends BaseService<TxResponderEntity> {

  public List<TxResponderPojo> queryList(Map<String, Object> map, Pagination pagination);

  List< TxResponderEntity> selectBySchoolId(String schools);

  public void deleteBatch(List<String> array);
}
