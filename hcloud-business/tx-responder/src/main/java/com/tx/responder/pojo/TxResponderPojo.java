package com.tx.responder.pojo;

import com.common.bean.Entity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:54
 */
@Table(name = "tx_responder")
@Builder
@Getter
@Setter
public class TxResponderPojo implements Entity {
    private static final long serialVersionUID = 1L;
    @Id
          /**
      *
       */
              private String id;
          /**
      *
       */
              private String testData;

    public TxResponderPojo() {
    }

    ;

    public TxResponderPojo(   String id ,   String testData  ) {
                    this.id= id;
                    this.testData= testData;
            }

    ;

  @Override
  @SneakyThrows
  public TxResponderPojo clone() {
    return (TxResponderPojo) super.clone();
  }

}
