package com.tx.responder.mapper;

import com.tx.responder.entity.TxResponderEntity;
import com.tx.responder.pojo.TxResponderPojo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author pengzhen
 * @email pengzhen
 * @date 2019-04-02 14:01:54
 */
@Mapper
public interface TxResponderMapper   extends tk.mybatis.mapper.common.Mapper<TxResponderEntity> {

  public List<TxResponderPojo> queryList(Map<String, Object> map);
  public List<TxResponderPojo> queryObject(String id);
  public List<TxResponderEntity> selectBySchoolId(String schools);
  public void deleteBatch(@Param("array") List<String> array);
}
