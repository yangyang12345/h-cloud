package com.hcloud.hcloudthread;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HcloudThreadApplication {

    public static void main(String[] args) {
        SpringApplication.run(HcloudThreadApplication.class, args);
    }

}
