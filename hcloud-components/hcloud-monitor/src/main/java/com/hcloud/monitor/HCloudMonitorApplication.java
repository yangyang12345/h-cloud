package com.hcloud.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author hepangui
 * @date 2018年11月26日
 * 监控
 */
@EnableDiscoveryClient
@EnableAdminServer
@SpringBootApplication
public class HCloudMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(HCloudMonitorApplication.class, args);
    }
}
