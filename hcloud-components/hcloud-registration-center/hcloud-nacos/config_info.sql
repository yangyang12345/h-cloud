/*
 Navicat Premium Data Transfer

 Source Server         : rm-bp12nf1usct07fs4lqo.mysql.rds.aliyuncs.com
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : rm-bp12nf1usct07fs4lqo.mysql.rds.aliyuncs.com:3306
 Source Schema         : nacos

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 17/04/2019 00:16:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (2, 'hcloud-audit-dev.yml', 'DEFAULT_GROUP', '## spring security 配置\r\nsecurity:\r\n  oauth2:\r\n    client:\r\n      client-id: audit\r\n      client-secret: ENC(cRL4Vx9PxjYDCNJLlPqOsg==)\r\n      scope: server\r\n\r\n# 数据源\r\nspring:\r\n  datasource:\r\n    url: jdbc:mysql://localhost:3306/hepgcloud?useUnicode=true&useSSL=false&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true\r\n    username: root\r\n    password: root\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n  jpa:\r\n    database-platform: com.hcloud.common.crud.config.MySql5InnonDbUtf8Dialect\r\n#    generate-ddl: true\r\n    show-sql: true\r\n    hibernate:\r\n      #自动更新表\r\n      ddl-auto: update\r\n      #字段驼峰映射\r\n      naming.physicalStrategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy\r\n\r\n', '4b3505dd7a650bdf0a5b53ebfda4074b', '2019-04-16 21:59:38', '2019-04-16 21:59:38', NULL, '175.13.253.159', '', '', 'hcloud-audit-dev.yml', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (3, 'hcloud-auth-dev.yml', 'DEFAULT_GROUP', 'security:\r\n  oauth2:\r\n    client:\r\n      client-id: auth\r\n      client-secret: ENC(cRL4Vx9PxjYDCNJLlPqOsg==)\r\n      scope: server\r\n\r\nspring.main.allow-bean-definition-overriding: true\r\n# 数据源\r\nspring:\r\n  datasource:\r\n    url: jdbc:mysql://localhost:3306/hepgcloud?useUnicode=true&useSSL=false&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true\r\n    username: root\r\n    password: ENC(Awm2vHHU8nZ6SxdETbiEdQ==)\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n  jpa:\r\n    database-platform: com.hcloud.common.crud.config.MySql5InnonDbUtf8Dialect\r\n    #    generate-ddl: true\r\n    show-sql: true\r\n    hibernate:\r\n      ddl-auto: update\r\n#      dialect.storage_engine: innondb\r\n#      dialect: org.hibernate.dialect.MySQL5InnoDBDialect\r\n\r\nhcloud.social:\r\n  qq:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  gitee:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  weixin:\r\n    app-id:\r\n    app-secret:\r\n\r\n', '1d8cba29b6426f6381c81389ecdee594', '2019-04-16 22:00:05', '2019-04-16 22:00:05', NULL, '175.13.253.159', '', '', 'hcloud-auth-dev.yml', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (4, 'hcloud-gateway-dev.yml', 'DEFAULT_GROUP', 'spring.main.allow-bean-definition-overriding: true\r\nspring:\r\n  cloud:\r\n    gateway:\r\n      locator:\r\n        enabled: true #服务名与地址转换开启\r\n      routes:\r\n      # 认证中心\r\n      - id: hcloud-auth\r\n        uri: lb://hcloud-auth\r\n        predicates:\r\n        - Path=/auth/**\r\n        filters:\r\n          - StripPrefix=1 #去掉auth前缀\r\n          # 验证码处理\r\n          - ImageCodeFilter\r\n        # 审计中心\r\n      - id: hcloud-audit\r\n        uri: lb://hcloud-audit\r\n        predicates:\r\n          - Path=/audit/**\r\n        filters:\r\n          - StripPrefix=1\r\n      #系统设置 模块\r\n      - id: hcloud-system\r\n        uri: lb://hcloud-system\r\n        predicates:\r\n        - Path=/system/**\r\n        filters:\r\n        - StripPrefix=1\r\n          # 限流配置\r\n#        - name: RequestRateLimiter\r\n#          args:\r\n#            key-resolver: \'#{@remoteAddrKeyResolver}\'\r\n#            redis-rate-limiter.replenishRate: 10\r\n#            redis-rate-limiter.burstCapacity: 20\r\n          # 降级配置\r\n        - name: Hystrix\r\n          args:\r\n            name: default\r\n            fallbackUri: \'forward:/fallback\'', '8d0c1f5bb8297ce7018abff14b88ae64', '2019-04-16 22:00:25', '2019-04-16 22:00:25', NULL, '175.13.253.159', '', '', 'hcloud-gateway-dev.yml', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (5, 'hcloud-system-dev.yml', 'DEFAULT_GROUP', '## spring security 配置\r\nsecurity:\r\n  oauth2:\r\n    client:\r\n      client-id: system\r\n      client-secret: ENC(cRL4Vx9PxjYDCNJLlPqOsg==)\r\n      scope: server\r\n\r\n# 数据源\r\nspring:\r\n  datasource:\r\n    url: jdbc:mysql://localhost:3306/hepgcloud?useUnicode=true&useSSL=false&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true\r\n    username: root\r\n    password: root\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n  jpa:\r\n    database-platform: com.hcloud.common.crud.config.MySql5InnonDbUtf8Dialect\r\n#    generate-ddl: true\r\n    show-sql: true\r\n    hibernate:\r\n      #自动更新表\r\n      ddl-auto: update\r\n      #字段驼峰映射\r\n      naming.physicalStrategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy\r\n\r\nhcloud.social:\r\n  qq:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  gitee:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  weixin:\r\n    app-id:\r\n    app-secret:', 'bf85b87a86598e8110dc4089e04bfd6b', '2019-04-16 22:00:43', '2019-04-16 22:00:43', NULL, '175.13.253.159', '', '', 'hcloud-system-dev.yml', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (6, 'application-dev.yml', 'DEFAULT_GROUP', '# 这是一个公共的配置文件，所有服务的公共配置放在这里\r\n\r\n# 加解密根密码\r\n# https://www.cnblogs.com/hfultrastrong/p/8556145.html\r\njasypt:\r\n  encryptor:\r\n    password: h-cloud #根密码\r\n# redis 相关\r\nspring:\r\n  redis:\r\n    password: redis\r\n    host: 192.168.2.50\r\n\r\n\r\n# feign 配置\r\nfeign:\r\n  hystrix:\r\n    enabled: true\r\n  okhttp:\r\n    enabled: false\r\n  httpclient:\r\n    enabled: true\r\n  client:\r\n    config:\r\n      feignName:\r\n        connectTimeout: 10000\r\n        readTimeout: 10000\r\n  compression:\r\n    request:\r\n      enabled: false\r\n    response:\r\n      enabled: true\r\n# hystrix If you need to use ThreadLocal bound variables in your RequestInterceptor`s\r\n# you will need to either set the thread isolation strategy for Hystrix to `SEMAPHORE or disable Hystrix in Feign.\r\nhystrix:\r\n  command:\r\n    default:\r\n      execution:\r\n        isolation:\r\n          strategy: SEMAPHORE\r\n          thread:\r\n            timeoutInMilliseconds: 60000\r\n  shareSecurityContext: true\r\n\r\n#请求处理的超时时间\r\nribbon:\r\n  ReadTimeout: 10000\r\n  ConnectTimeout: 10000\r\n\r\n## spring security 配置\r\nsecurity:\r\n  auth:\r\n    server: http://localhost/auth/oauth\r\n  oauth2:\r\n    resource:\r\n      token-info-uri: ${security.auth.server}/check_token\r\n\r\n#出现错误时, 直接抛出异常\r\nspring.mvc.throw-exception-if-no-handler-found: true\r\n#不要为我们工程中的资源文件建立映射\r\nspring.resources.add-mappings: false\r\n\r\n\r\n# 暴露监控端点\r\nmanagement.endpoints.web.exposure.include: \'*\'\r\nhcloud:\r\n  oauth:\r\n    token:\r\n      type: redis\r\n  log:\r\n    login:\r\n      enable: false\r\n    operate:\r\n      enable: true\r\n      query: false\r\n\r\n', '11775ae54f86efab58bfed71d3832cc9', '2019-04-16 22:01:22', '2019-04-16 22:01:22', NULL, '175.13.253.159', '', '', 'application-dev.yml', NULL, NULL, 'yaml', NULL);

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime(0) NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(64) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES (0, 2, 'hcloud-audit-dev.yml', 'DEFAULT_GROUP', '', '## spring security 配置\r\nsecurity:\r\n  oauth2:\r\n    client:\r\n      client-id: audit\r\n      client-secret: ENC(cRL4Vx9PxjYDCNJLlPqOsg==)\r\n      scope: server\r\n\r\n# 数据源\r\nspring:\r\n  datasource:\r\n    url: jdbc:mysql://localhost:3306/hepgcloud?useUnicode=true&useSSL=false&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true\r\n    username: root\r\n    password: root\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n  jpa:\r\n    database-platform: com.hcloud.common.crud.config.MySql5InnonDbUtf8Dialect\r\n#    generate-ddl: true\r\n    show-sql: true\r\n    hibernate:\r\n      #自动更新表\r\n      ddl-auto: update\r\n      #字段驼峰映射\r\n      naming.physicalStrategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy\r\n\r\n', '4b3505dd7a650bdf0a5b53ebfda4074b', '2010-05-05 00:00:00', '2019-04-16 21:59:38', NULL, '175.13.253.159', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 3, 'hcloud-auth-dev.yml', 'DEFAULT_GROUP', '', 'security:\r\n  oauth2:\r\n    client:\r\n      client-id: auth\r\n      client-secret: ENC(cRL4Vx9PxjYDCNJLlPqOsg==)\r\n      scope: server\r\n\r\nspring.main.allow-bean-definition-overriding: true\r\n# 数据源\r\nspring:\r\n  datasource:\r\n    url: jdbc:mysql://localhost:3306/hepgcloud?useUnicode=true&useSSL=false&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true\r\n    username: root\r\n    password: ENC(Awm2vHHU8nZ6SxdETbiEdQ==)\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n  jpa:\r\n    database-platform: com.hcloud.common.crud.config.MySql5InnonDbUtf8Dialect\r\n    #    generate-ddl: true\r\n    show-sql: true\r\n    hibernate:\r\n      ddl-auto: update\r\n#      dialect.storage_engine: innondb\r\n#      dialect: org.hibernate.dialect.MySQL5InnoDBDialect\r\n\r\nhcloud.social:\r\n  qq:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  gitee:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  weixin:\r\n    app-id:\r\n    app-secret:\r\n\r\n', '1d8cba29b6426f6381c81389ecdee594', '2010-05-05 00:00:00', '2019-04-16 22:00:05', NULL, '175.13.253.159', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 4, 'hcloud-gateway-dev.yml', 'DEFAULT_GROUP', '', 'spring.main.allow-bean-definition-overriding: true\r\nspring:\r\n  cloud:\r\n    gateway:\r\n      locator:\r\n        enabled: true #服务名与地址转换开启\r\n      routes:\r\n      # 认证中心\r\n      - id: hcloud-auth\r\n        uri: lb://hcloud-auth\r\n        predicates:\r\n        - Path=/auth/**\r\n        filters:\r\n          - StripPrefix=1 #去掉auth前缀\r\n          # 验证码处理\r\n          - ImageCodeFilter\r\n        # 审计中心\r\n      - id: hcloud-audit\r\n        uri: lb://hcloud-audit\r\n        predicates:\r\n          - Path=/audit/**\r\n        filters:\r\n          - StripPrefix=1\r\n      #系统设置 模块\r\n      - id: hcloud-system\r\n        uri: lb://hcloud-system\r\n        predicates:\r\n        - Path=/system/**\r\n        filters:\r\n        - StripPrefix=1\r\n          # 限流配置\r\n#        - name: RequestRateLimiter\r\n#          args:\r\n#            key-resolver: \'#{@remoteAddrKeyResolver}\'\r\n#            redis-rate-limiter.replenishRate: 10\r\n#            redis-rate-limiter.burstCapacity: 20\r\n          # 降级配置\r\n        - name: Hystrix\r\n          args:\r\n            name: default\r\n            fallbackUri: \'forward:/fallback\'', '8d0c1f5bb8297ce7018abff14b88ae64', '2010-05-05 00:00:00', '2019-04-16 22:00:25', NULL, '175.13.253.159', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 5, 'hcloud-system-dev.yml', 'DEFAULT_GROUP', '', '## spring security 配置\r\nsecurity:\r\n  oauth2:\r\n    client:\r\n      client-id: system\r\n      client-secret: ENC(cRL4Vx9PxjYDCNJLlPqOsg==)\r\n      scope: server\r\n\r\n# 数据源\r\nspring:\r\n  datasource:\r\n    url: jdbc:mysql://localhost:3306/hepgcloud?useUnicode=true&useSSL=false&characterEncoding=utf-8&allowMultiQueries=true&autoReconnect=true\r\n    username: root\r\n    password: root\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    driver-class-name: com.mysql.jdbc.Driver\r\n  jpa:\r\n    database-platform: com.hcloud.common.crud.config.MySql5InnonDbUtf8Dialect\r\n#    generate-ddl: true\r\n    show-sql: true\r\n    hibernate:\r\n      #自动更新表\r\n      ddl-auto: update\r\n      #字段驼峰映射\r\n      naming.physicalStrategy: org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy\r\n\r\nhcloud.social:\r\n  qq:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  gitee:\r\n    app-id:\r\n    app-secret:\r\n    redirect-uri: http://cloud.hepg.net/login.html\r\n  weixin:\r\n    app-id:\r\n    app-secret:', 'bf85b87a86598e8110dc4089e04bfd6b', '2010-05-05 00:00:00', '2019-04-16 22:00:43', NULL, '175.13.253.159', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 6, 'application-dev.yml', 'DEFAULT_GROUP', '', '# 这是一个公共的配置文件，所有服务的公共配置放在这里\r\n\r\n# 加解密根密码\r\n# https://www.cnblogs.com/hfultrastrong/p/8556145.html\r\njasypt:\r\n  encryptor:\r\n    password: h-cloud #根密码\r\n# redis 相关\r\nspring:\r\n  redis:\r\n    password: redis\r\n    host: 192.168.2.50\r\n\r\n\r\n# feign 配置\r\nfeign:\r\n  hystrix:\r\n    enabled: true\r\n  okhttp:\r\n    enabled: false\r\n  httpclient:\r\n    enabled: true\r\n  client:\r\n    config:\r\n      feignName:\r\n        connectTimeout: 10000\r\n        readTimeout: 10000\r\n  compression:\r\n    request:\r\n      enabled: false\r\n    response:\r\n      enabled: true\r\n# hystrix If you need to use ThreadLocal bound variables in your RequestInterceptor`s\r\n# you will need to either set the thread isolation strategy for Hystrix to `SEMAPHORE or disable Hystrix in Feign.\r\nhystrix:\r\n  command:\r\n    default:\r\n      execution:\r\n        isolation:\r\n          strategy: SEMAPHORE\r\n          thread:\r\n            timeoutInMilliseconds: 60000\r\n  shareSecurityContext: true\r\n\r\n#请求处理的超时时间\r\nribbon:\r\n  ReadTimeout: 10000\r\n  ConnectTimeout: 10000\r\n\r\n## spring security 配置\r\nsecurity:\r\n  auth:\r\n    server: http://localhost/auth/oauth\r\n  oauth2:\r\n    resource:\r\n      token-info-uri: ${security.auth.server}/check_token\r\n\r\n#出现错误时, 直接抛出异常\r\nspring.mvc.throw-exception-if-no-handler-found: true\r\n#不要为我们工程中的资源文件建立映射\r\nspring.resources.add-mappings: false\r\n\r\n\r\n# 暴露监控端点\r\nmanagement.endpoints.web.exposure.include: \'*\'\r\nhcloud:\r\n  oauth:\r\n    token:\r\n      type: redis\r\n  log:\r\n    login:\r\n      enable: false\r\n    operate:\r\n      enable: true\r\n      query: false\r\n\r\n', '11775ae54f86efab58bfed71d3832cc9', '2010-05-05 00:00:00', '2019-04-16 22:01:22', NULL, '175.13.253.159', 'I', '');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', 1);

SET FOREIGN_KEY_CHECKS = 1;
