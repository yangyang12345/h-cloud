package com.hcloud.auth.config;

/**
 * 安全认证模块的常量接口类,里面包含所有要用到的常量,方便以后维护和扩展
 * @author hepangui
 * @date 2018-12-13
 */
public interface SecurityConstants {

    /**
     * 账号密码登录
     */
    String LOGIN_PASS = "/auth/pass";
    /**
     * 手机，微信，qq登录
     */
    String LOGIN_THRID = "/login/*";
    String LOGIN_MOBILE = "/login/mobile";
    String LOGIN_WEIXIN = "/login/weixin";
    String LOGIN_QQ = "/login/qq";
    String LOGIN_GITEE = "/login/gitee";
    /**
     * 发送短信验证码 或 验证短信验证码时，传递手机号的参数的名称
     */
    String PARAM_MOBILE = "mobile";
    String PARAM_CODE = "code";
    String PARAM_SMS_CODE = "smsCode";

    /**
     * 监控节点
     */
    String MONITOR_URL = "/actuator/**";


}
