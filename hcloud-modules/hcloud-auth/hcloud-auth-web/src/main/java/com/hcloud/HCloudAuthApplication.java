package com.hcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * @author hepangui
 * @date 2018年10月26日
 * 认证中心
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAuthorizationServer
public class HCloudAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(HCloudAuthApplication.class, args);
    }
}
