package com.hcloud.audit.api.feign;

import com.hcloud.audit.api.bean.LoginLog;
import com.hcloud.audit.api.bean.OperateLog;
import com.hcloud.audit.api.feign.factory.RemoteLogFallbackFactory;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.constants.ServerNameContants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@FeignClient(value = ServerNameContants.AUDIT, fallbackFactory = RemoteLogFallbackFactory.class)
public interface RemoteLogService {
    @PostMapping(value = "/operate/save")
    HCloudResult saveLog(@RequestBody OperateLog operateLog);

    @PostMapping(value = "/login/save")
    HCloudResult saveLoginLog(@RequestBody LoginLog loginLog);
}
