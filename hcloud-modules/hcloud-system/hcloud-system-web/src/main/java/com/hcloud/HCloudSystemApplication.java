package com.hcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author hepg
 * @date 2018年10月26日
 * 服务中心
 */
@SpringBootApplication
@EnableDiscoveryClient
public class HCloudSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(HCloudSystemApplication.class, args);
    }

}
