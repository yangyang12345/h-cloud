package com.hcloud.system.api.feign.user;

import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.core.base.User;
import com.hcloud.common.core.constants.ServerNameContants;
import com.hcloud.system.api.feign.user.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@FeignClient(value = ServerNameContants.SYSTEM, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService   {
    @GetMapping("/user/getByAccount/{account}")
    HCloudResult<User> getUserByAccount(@PathVariable("account") String account);

    @GetMapping("/user/getByMobile/{para}")
    HCloudResult<User> getUserByMobile(@PathVariable("para") String para);

    @GetMapping("/user/getByQq/{para}")
    HCloudResult<User> getUserByQq(@PathVariable("para") String para);

    @GetMapping("/user/getByWeixin/{para}")
    HCloudResult<User> getUserByWeixin(@PathVariable("para") String para);
}
